import Vue from "vue";
import App from "./App.vue";

import vuelidate from 'vuelidate';
import InlineSvg from 'vue-inline-svg';
import {mask} from 'vue-the-mask'
import Datepicker from 'v-calendar/lib/components/date-picker.umd';

import router from "./router";
import store from './Store/index';

Vue.use(vuelidate);

Vue.directive('mask', mask);

Vue.component('inline-svg', InlineSvg);

Vue.component('date-picker', Datepicker)

Vue.config.productionTip = false;

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount("#app");
