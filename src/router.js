import Vue from 'vue';
import Router from 'vue-router';

import dashboard from './views/Dashboard';
import { auth } from './firebase';

Vue.use(Router);

const router = new Router({
    mode: 'history',

    routes: [
        {
            path: '/dashboard',
            component: dashboard,
            meta: {
                requiresAuth: true
            },
            children: [
                {
                    path: '',
                    component: () => import(/* webpackChunkName: "Overview"*/"./views/Overview.vue"),
                    children: [
                        {
                            path: '',
                            component: () => import(/* webpackChunkName: "OverviewDetails"*/"./components/OverviewDetails.vue"),
                        },
                        {
                            path: 'timeline',
                            component: () => import(/* webpackChunkName: "TimelineDetails" */ "./components/TimelineDetails.vue")
                        }
                    ]
                },
                {
                    path: '/messages',
                    component: () => import(/* webpackChunkName: "Messages"*/ "./views/Messages.vue"),
                },
                {
                    path: '/diets',
                    component: () => import(/* webpackChunkName: "Diets"*/ "./views/Diets.vue")
                },
                {
                    path: '/calculators',
                    component: () => import(/* webpackChunkName: "Calculators" */ "./views/Calculators.vue")
                },
                {
                    path: '/profile',
                    component: () => import(/* webpackChunkName: "Profile" */ "./views/Profile.vue")
                },
                {
                    path: '/settings',
                    component: () => import(/* webpackChunkName: "Settings" */ "./views/Settings.vue")
                },
                {
                    path: '/addplan',
                    component: () => import(/* webpackChunkName: "AddPlan" */ "./views/AddPlan.vue")
                },
                {
                    path: '/addproduct',
                    component: () => import(/* webpackChunkName: "AddProducts" */ "./views/AddProducts.vue")
                }
            ]
        },
        {
            path: '/login',
            component: () => import(/* webpackChunkName: "Login/Signup"*/ "./views/Signuplogin.vue"),
            children: [
                {
                    path: '',
                    component: () => import(/* webpackChunkName: "Login"*/ "./components/Login.vue"),
                    name: 'login'
                },
                {
                    path: '/signup',
                    component: () => import(/* webpackChunkName: "Signup"*/ "./components/Signup.vue"),
                    name: 'signup'
                }
            ]
        },
        {
            path: '/signupsteps',
            component: () => import(/* webpackChunkName: "SignupSteps" */ "./views/SignupSteps.vue"),
            name: 'signupSteps',
            meta: {
                requiresAuth: true
            }
        }
    ]
});

router.beforeEach((to, from, next) => {
    const requiresAuth = to.matched.some(x => x.meta.requiresAuth)

    if (requiresAuth) {
        auth.onAuthStateChanged(user => {
            if(!user) {
                next({ name: 'login' })
            } else {
                next()
            }
        })
    } else {
        auth.onAuthStateChanged(user => {
            if (user) {
                next()
            } else {
                next()
            }
        })
    }
})

export default router