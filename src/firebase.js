import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/firebase-storage"

const firebaseConfig = {
    apiKey: "AIzaSyAV4Jd8Q5L6M8jWwwlXZWCPB53C7Vuqc-8",
    authDomain: "preppt-app.firebaseapp.com",
    databaseURL: "https://preppt-app.firebaseio.com",
    projectId: "preppt-app",
    storageBucket: "preppt-app.appspot.com",
    messagingSenderId: "1074796412358",
    appId: "1:1074796412358:web:0fb0ae451b689bd34486c7"
};

firebase.initializeApp(firebaseConfig);

const auth = firebase.auth()
const db = firebase.firestore()
const storage = firebase.storage()

export { auth, db, storage }