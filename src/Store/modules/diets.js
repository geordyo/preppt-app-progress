import { auth, db } from '../../firebase'

const diets = {
    namespaced: false,

    state: {
        dietTypes: [
            'Custom',
            'Ketogenic (Keto)',
            'Paleo',
            'Vegan',
            'Low-Carb',
            'Mediterranean'
        ],
    },

    mutations: {
        
    },

    actions: {
        async submitDietPlan ({ dispatch }, dietPlan) {
            const dietName = dietPlan.dietName
            const uid = auth.currentUser.uid
            await db.collection("users").doc(uid).collection("userDiets").doc(dietName).set(dietPlan)
                .then(() => {
                    // adds new meal to local storage as well
                    dispatch('getUserOwnDiets')
                })
        }
    },

    getters: {
        getDietTypes (state) {
            return state.dietTypes
        },
    }
}

export default diets