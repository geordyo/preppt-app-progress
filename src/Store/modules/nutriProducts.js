import { db } from '../../firebase'
// import Vue from 'vue'

// Locally stored data
const allProducts = window.localStorage.getItem('persistAllProducts')

const nutritionalProducts = {
    namespaced: false,
    
    state: {
        allProducts: allProducts ? JSON.parse(allProducts) : [],
        products: [],
        dietType: '',
        userAllergents: [],
        selectedCalorieRange: [],
        addNewMealData: {
            selectedProducts: [],
            totals: {
                protein: 0,
                carbs: 0,
                fats: 0,
                energy: 0
            }
        },
        allergents: [
            'Cows Milk',
            'Eggs',
            'Tree Nuts',
            'Fish',
            'Avocado',
            'Soy',
            'Garlic',
            'Peanuts',
            'Shellfish',
            'Wheat',
            'Gluten'
        ],
        calorieRange: [
            '0 +',
            '0 - 500',
            '500 - 1000',
            '1000 +'
        ]
    },

    mutations: {
        SET_ALL_PRODUCTS (state, allProducts) {
            state.allProducts = allProducts
            window.localStorage.setItem('persistAllProducts', JSON.stringify(allProducts))
        },

        SET_SEARCH_PARAMS (state, searchParamDietType) {
            state.dietType = searchParamDietType
        },

        SET_USER_ALLERGIES (state, allergentArr) {
            state.userAllergents = [state.allergents, ...allergentArr]
        },

        SET_SEARCH_PARAM_PRODUCTS (state, products) {
            state.products = products
        },

        SET_TO_SELECTED_PRODUCTS (state, product) {
            const newProduct = product.item
            newProduct.id = product.itemId
            state.addNewMealData.selectedProducts.push(newProduct)
        },

        SET_SELECTED_MEAL_TOTALS (state, totals) {
            state.addNewMealData.totals = totals
        },

        REMOVE_FROM_SELECTED_PRODUCTS (state, product) {
            state.addNewMealData.selectedProducts = state.addNewMealData.selectedProducts.filter(curProduct => {
                return curProduct.name !== product.name
            })
        },

        ADD_TO_MEAL_TOTALS (state, selectedProduct) {
            Object.keys(state.addNewMealData.totals).forEach(key => {
                return state.addNewMealData.totals[key] = 0
            })
            const index = state.addNewMealData.selectedProducts.findIndex(product => product.name === selectedProduct.name)
            state.addNewMealData.selectedProducts.splice(index, 1, selectedProduct)
            
            state.addNewMealData.selectedProducts.forEach(product => {
                state.addNewMealData.totals.protein += Number(product.macros.protein.split(" ")[0])
                state.addNewMealData.totals.fats += Number(product.macros.fats.split(" ")[0])
                state.addNewMealData.totals.carbs += Number(product.macros.carbs.split(" ")[0])
                state.addNewMealData.totals.energy += Number(product.energy.split(" ")[0])
            })
        },

        REMOVE_FROM_MEAL_TOTALS (state, product) {
            Object.keys(state.addNewMealData.totals).forEach(key => {
                for (let macro in product.macros) {
                    if (macro === key) {
                        state.addNewMealData.totals[key] -= parseFloat(product.macros[macro].split(' ')[0])
                    }
                }

                if (key === 'energy') {
                    state.addNewMealData.totals.energy -= parseFloat(product.energy.split(' ')[0])
                }
            })
        },

        UPDATE_MEAL_TOTALS (state) {
            state.addNewMealData.selectedProducts.forEach(product => {
                state.addNewMealData.totals.protein = product.macros.protein
                state.addNewMealData.totals.fats = product.macros.protein
                state.addNewMealData.totals.carbs = product.macros.protein
                state.addNewMealData.totals.energy = product.macros.protein
            })
        },

        UPDATE_PRODUCT_NUTRI_VALS (state, selectedProduct) {
            if (state.addNewMealData.selectedProducts.length === 0) {
                state.addNewMealData.selectedProducts = []
                Object.keys(state.addNewMealData.totals).forEach(key => {
                    return state.addNewMealData.totals[key] = 0 
                })
            } else {
                Object.keys(state.addNewMealData.totals).forEach(key => {
                    return state.addNewMealData.totals[key] = 0 
                })
                const index = state.addNewMealData.selectedProducts.findIndex(product => product.name === selectedProduct.name)
                state.addNewMealData.selectedProducts.splice(index, 1, selectedProduct)
                
                state.addNewMealData.selectedProducts.forEach(product => {
                    state.addNewMealData.totals.protein += Number(product.macros.protein.split(" ")[0])
                    state.addNewMealData.totals.fats += Number(product.macros.fats.split(" ")[0])
                    state.addNewMealData.totals.carbs += Number(product.macros.carbs.split(" ")[0])
                    state.addNewMealData.totals.energy += Number(product.energy.split(" ")[0])
                })
            }
        },

        SET_SELECTED_PRODUCTS (state, products) {
            state.addNewMealData.selectedProducts = products
        },

        CLEAR_MEAL_DATA (state) {
            state.addNewMealData.selectedProducts = []
            Object.keys(state.addNewMealData.totals).forEach(key => {
                return state.addNewMealData.totals[key] = 0 
            })
        }
    },

    actions: {
        async registerNewProduct (context, productDetails) {
            const name = productDetails.name
            await db.collection("nutritionalItems").doc(name).set(productDetails)
        },

        async getProductsFromDB ({ commit, state }, term = 'default') {
            if (term === 'withParams' || state.allProducts.length === 0) {
                await db.collection("nutritionalItems")
                .where('dietTypeName', 'array-contains', state.dietType)
                .get()
                .then(querySnapshot => {
                    let allProducts = []
                    querySnapshot.forEach(doc => {
                        allProducts.push({ name: doc.name, ...doc.data() })
                    })
                    commit('SET_ALL_PRODUCTS', allProducts)
                })
            } else if (state.allProducts.length !== 0) {
                commit('SET_ALL_PRODUCTS', state.allProducts)
            }
        },

        getAllergyParamProducts ({ commit }, allergentArr) {
            commit('SET_USER_ALLERGIES', allergentArr)
            console.log(allergentArr)
        },

        setAllergyParamProducts ({ commit, state }) {
            const allergyFilteredProducts = []
            state.allProducts.forEach(product => {
                if (product.allergies.some(allergy => state.userAllergents.includes(allergy))) {
                    return
                } else {
                    allergyFilteredProducts.push(product)
                }
            })
            commit('SET_SEARCH_PARAM_PRODUCTS', allergyFilteredProducts)
        },

        getProductsFromRange ({ commit, state }, range) {
            let selectedRange = range.split(' ')
            let rangeFilteredProducts = []
            state.products.forEach(product => {
                let selectedProductEnergy = parseFloat(product.energy.split(' ')[0])
                if (selectedRange.length === 3) {
                    if (selectedProductEnergy > parseFloat(selectedRange[0]) && selectedProductEnergy < parseFloat(selectedRange[selectedRange.length - 1])) {
                        rangeFilteredProducts.push(product)
                    } else {
                        return
                    }
                } else if (selectedRange.length === 2) {
                    if (selectedProductEnergy > parseFloat(selectedRange[0])) {
                        rangeFilteredProducts.push(product)
                    } else {
                        return
                    }
                }
            })
            commit('SET_SEARCH_PARAM_PRODUCTS', rangeFilteredProducts)
        },

        addToSelectedProducts ({ commit }, { item, itemId }) {
            console.log(itemId)
            console.log(item)
            commit('SET_TO_SELECTED_PRODUCTS', { item, itemId })
            commit('ADD_TO_MEAL_TOTALS', item)
        },

        setSelectedProducts ({ commit }, products) {
            commit('SET_SELECTED_PRODUCTS', products)
        },

        setSelectedTotals ({ commit }, totals) {
            commit('SET_SELECTED_MEAL_TOTALS', totals)
        },

        removeFromSelectedProducts ({ commit }, product) {
            commit('REMOVE_FROM_SELECTED_PRODUCTS', product)
            commit('REMOVE_FROM_MEAL_TOTALS', product)
        },

        updateMealTotals ({ commit }, product) {
            commit('UPDATE_MEAL_TOTALS', product)
        }
    },

    getters: {
        getAllProducts (state) {
            return state.products
        },

        getProductsFromCat (state) {
            return state.userAllergents.length
        },

        getUserAllergents (state) {
            return state.userAllergents
        },

        getAllAllergents (state) {
            return state.allergents
        },

        getCalorieRange (state) {
            return state.calorieRange
        },

        getMealSelectedProducts (state) {
            return state.addNewMealData.selectedProducts
        },

        getMealTotals (state) {
            return state.addNewMealData.totals
        }
    }
}

export default nutritionalProducts