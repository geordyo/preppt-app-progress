import { auth, db, storage } from '../../firebase'
const userData = window.localStorage.getItem('persistUserData') // LS for local storage
const userProfileImg = window.localStorage.getItem('persistUserImg')
const userOwnDiets = window.localStorage.getItem('persistUserOwnDiets')

const currentUser = {
    namespaced: false,

    state: {
        userData: userData ? JSON.parse(userData) : {},
        profileImg: userProfileImg ? JSON.parse(userProfileImg) : "",
        ownDiets: userOwnDiets ? JSON.parse(userOwnDiets) : []
    },

    mutations: {
        SET_USER_DATA (state, form) {
            Object.assign(state.userData, form)
            window.localStorage.setItem('persistUserData', JSON.stringify(form))
        },

        SET_USER_PROFILEPIC (state, pic) {
            state.profileImg = pic
            window.localStorage.setItem('persistUserImg', JSON.stringify(pic))
        },

        SET_USER_OWN_DIETS (state, data) {
            state.ownDiets = data
            window.localStorage.setItem('persistUserOwnDiets', JSON.stringify(data))
        }
    },

    actions: {
        async registerUserData ({ commit, dispatch }, userDetails) {
            const userId = auth.currentUser.uid
            await db.collection("users").doc(userId).set(userDetails)
            commit('SET_USER_DATA', userDetails)
            dispatch('RegisterUserProfilePic', userDetails.profilePic)
            dispatch('getOwnDiets')
            
        },

        async RegisterUserProfilePic ({ commit }, pic) {
            const userId = auth.currentUser.uid
            await storage.ref().child('userProfileImgs/' + userId + '/profile.jpg').putString(pic, 'base64')
            const pathRef = storage.ref('userProfileImgs/' + userId + '/profile.jpg')
            pathRef.getDownloadURL()
                .then(imgUrl => {
                    commit('SET_USER_PROFILEPIC', imgUrl)
                })
        },

        async getUserData ({ commit, dispatch, state }) {
            if (state.userData.length === 0) {
                return
            } else {
                const userId = auth.currentUser.uid
                await db.collection("users").doc(userId).get()
                    .then(doc => {
                        commit('SET_USER_DATA', doc.data())
                    })
                    .catch(err => {
                        console.log(err)
                    })
                dispatch('getUserProfilePic')
                dispatch('getUserOwnDiets')
            }
        },

        async getUserProfilePic ({ commit }) {
            const userId = auth.currentUser.uid
            await storage.ref('userProfileImgs/' + userId + '/profile.jpg').getDownloadURL()
                .then(imgUrl => {
                    commit('SET_USER_PROFILEPIC', imgUrl)
                })
                .catch(err => {
                    console.log(err)
                })
        },

        async getUserOwnDiets ({ commit }) {
            const userId = auth.currentUser.uid
            await db.collection("users").doc(userId).collection("userDiets").get()
                .then((querySnapshot) => {
                    const allDiets = []
                    querySnapshot.forEach((doc) => {
                        allDiets.push({ id: doc.id, ...doc.data() })
                    });
                    commit('SET_USER_OWN_DIETS', allDiets)
                });
        }
    },

    getters: {
        getUserName (state) {
            return state.userData.fullName
        },

        getUserImg (state) {
            return state.profileImg
        },

        getAllUserData (state) {
            return state.userData
        },

        getUserOwnDiets (state) {
            return state.ownDiets
        }
    }
}

export default currentUser