import Vue from 'vue';
import Vuex from 'vuex';

import currentUser from './modules/currentUser';
import diets from './modules/diets';
import nutritionalProducts from './modules/nutriProducts'

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        currentUser,
        diets,
        nutritionalProducts
    }
});

export default store